import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {AuthServiceConfig, 
        GoogleLoginProvider, 
        FacebookLoginProvider, SocialLoginModule} from "angular-6-social-login";
import { SigninComponent } from './signin/signin.component';
import { RouterModule, Routes } from '@angular/router';
import { DetailsComponent } from './details/details.component';

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider("2013050615392888")
        },
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("224889169940-kltbp3595h4sf82oj92h69h11gkqle5v.apps.googleusercontent.com")
        },
      ]
  );
  return config;
}

const routes : Routes = [
  {
  path: '',
  component: SigninComponent
  },
  {
    path: 'details',
    component: DetailsComponent
    }
]

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    DetailsComponent
  ],
  imports: [
    BrowserModule,
    SocialLoginModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
  {
    provide: AuthServiceConfig,
    useFactory: getAuthServiceConfigs
  }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
