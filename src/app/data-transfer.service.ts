import { Injectable } from '@angular/core';
import { Userdetails } from './userdetails';

@Injectable({
  providedIn: 'root'
})
export class DataTransferService {
details = new Userdetails();
  constructor() { }

  SetData (data): void {
    this.details = data;
  }
  GetData (): Userdetails {
    return this.details;
  }
}
