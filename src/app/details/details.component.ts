import { Component, OnInit } from '@angular/core';
import { DataTransferService} from '../data-transfer.service';
import { Userdetails} from '../userdetails';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
details = new Userdetails();
  constructor(private dt: DataTransferService) { }

  
  ngOnInit() {
    this.details = this.dt.GetData();
  }

}
