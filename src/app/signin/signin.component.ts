import { Component, OnInit } from '@angular/core';
import  { DataTransferService } from '../data-transfer.service'
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider, 
} from 'angular-6-social-login';
import { Userdetails } from '../userdetails';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent {
data = new Userdetails();
  constructor( private socialAuthService: AuthService, private dt: DataTransferService,
  private routes: Router ) {}
  
  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    } 
    
    this.socialAuthService.signIn(socialPlatformProvider).then(
    
      (userData) => {
        this.dt.SetData(userData);
        this.routes.navigate(['/details'])
      }
      // (userData) => {
      //   console.log(socialPlatform+" sign in data : " , userData);
      //   // Now sign-in with userData
      // //   // ...
            
      // }
      
    );
  }
}
